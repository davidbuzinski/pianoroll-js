import React, { useState, useEffect } from 'react';
import * as Tone from 'tone';
import Container from './Container.js'
import Menu from './Menu.js'
import toggleAudio from './toggleAudio'
import './App.css';

var identifier = 1

function App({numRows, numCols}) {
  const [notes, setNotes] = useState([]);
  const [playing, setPlaying] = useState(false);
  const [bpm, setBpm] = useState(120);

  const createNote = (event,row,leftEnd,rightEnd) => {
    setNotes(notes.concat([{leftEnd:leftEnd, rightEnd:rightEnd, row:row, id:identifier++}]))
    setPlaying(false)
  }
  
  const deleteNote = (event,id) => {
    setNotes(notes.filter(x => x.id !== id))
    setPlaying(false)
  }
  
  const moveNote = (id,newRow,newLeft,newRight) => {
    this.deleteNote(undefined,id)
    this.createNote(undefined,newRow,newLeft,newRight)
  }
  
  const extendNote = (note,side,newEnd) => {
    if (side === 'left') {
      return Object.assign({},note,{leftEnd:newEnd})
    } else if (side === 'right') {
      return Object.assign({},note,{rightEnd:newEnd})
    } else {
      return note
    }
  }

  useEffect(() => { 
    toggleAudio(notes, bpm, playing);
    return function cleanup() { 
      Tone.Transport.stop()
      Tone.Transport.cancel()
    };
  });
  
  return (
    <div className="App">
      <Menu
        playing = {playing}
        bpm = {bpm}
        setBpm = {setBpm}
        setPlaying = {setPlaying}
        />
    <div>
      <div id="playhead"
        className = {playing ? 'movingPlayhead' : '' }
        style={{animationDuration: (960/bpm) + 's'}}
        />
      <Container
        numRows = {numRows}
        numCols = {numCols}
        createNote = {createNote}
        deleteNote = {deleteNote}
        moveNote = {moveNote}
        extend = {extendNote}
        notes = {notes}
        />
    </div>
  </div>

  )
}

export default App
