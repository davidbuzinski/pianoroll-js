import React, {Component} from 'react';
import Note from './Note'

class Container extends Component {

  createCells = () => {
    const cells = []
    for (let row=1;row<=this.props.numRows;row++) {
      for (let col=1;col<=this.props.numCols;col++) {
        cells.push(<div className = "ContainerCell"
          onClick = {(e) => this.props.createNote(e,row,col,col)}
          style = {{gridRow:row,gridColumn:col}}
          key = {row+','+col}/>)
      }
    }
    return cells
  }

  render () {

    const styling = {
      gridTemplateRows:"repeat( "+this.props.numRows +",1fr)",
      gridTemplateColumns:"repeat( "+this.props.numCols + ",1fr)",
    }

    return ( 
    <div className = "Container"
      style = {styling}>
      {this.createCells().concat(
        this.props.notes.map(x => <Note
          leftEnd={x.leftEnd}
          rightEnd={x.rightEnd}
          row={x.row}
          id={x.id}
          deleteNote={this.props.deleteNote}
          key = {'note ' + x.id}/>)
      )}
    </div>
  )}
}

export default Container;
