import React, {Component} from 'react';
import NoteExtender from './NoteExtender'

class Note extends Component {

  render(){

    const {leftEnd, rightEnd, row, id, deleteNote} = this.props
    const styling = {
      gridRow:row,
      gridColumn : leftEnd +'/'+ (rightEnd+1),
    }

    return (
      <div className="Note"
        style = {styling}
        onClick = {(e)=> deleteNote(e,id)}>
        <NoteExtender id = {id}
          side = 'left'
          leftEnd = {leftEnd}
          rightEnd = {rightEnd}
          row = {row}
          />
        <NoteExtender id = {id}
          side = 'right'
          leftEnd = {leftEnd}
          rightEnd = {rightEnd}
          row = {row}
          />
      </div>
    )
  }
}

export default Note;
